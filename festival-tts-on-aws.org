#+title: Install Festival Text to Speech on Amazon Linux 2
#+author: Chen Rotem Levy
#+email: chen@rotemlevy.name
#+options: ^:nil toc:2

**Stauts: WIP**

* Abstract

  This document describes how to setup a text-to-speech (TTS) as an accessibility aid on an *Amazon Linux 2* distribution running as a desktop via [[https://aws.amazon.com/workspaces/][AWS Workspaces]].

  The solution is based on the [[http://www.cstr.ed.ac.uk/projects/festival/][Festival]] project from the University Edinburgh, and an British English voice from the [[https://github.com/numediart/MBROLA][Mrola]] project.

  It is wrapped by a set of scripts I cobbled together that is [[https://gitlab.com/chenlevy/dotfiles/-/tree/master/tts/bin/tts][part of my dot-files]], and use dbus to pipe the data from the clipboard to the TTS engine.

* Download

  *Amazon Linux 2* is originally [[https://serverfault.com/questions/798427][based on RHEL5/6 but now resembles RHEL7]], although as a desktop distribution is a bit more restricted and stripped down, even after considering, so [[https://kde.org][KDE Plasma]] (that was part of my original solution) is not available. Its repositories (even when considering [[https://aws.amazon.com/premiumsupport/knowledge-center/ec2-install-extras-library-software/][amazon-linux-extras]]), are fairly limited, and thus we will need to install the TTS without the aid of the package manager:

  The US mirror holds a later version of the *Festival engine*:
  [[http://festvox.org/packed/festival/2.5/]]

  Get the *Mborola en1* voice from:
  [[https://github.com/numediart/MBROLA-voices/blob/master/data/en1/en1?raw=true]]

* Limitations and Workarounds

** PCoIP Linux Client

  The Linux *PC over IP* (pcoip) client for AWS Workspaces, is a bit clunky, so whenever I needed to change the client's window size (including going in and out of full screen mode), the resolution of the remote system got distorted, and the mouse point and got misaligned with the rendering of the mouse pointer.

  I had to disconnect and reconnect to the session for the resolution to get back to a proper working condition.

** *Amazon Linux 2* Limitations

   The options of OS using

  - Copy JS Widget does not work - mark the text manually

  - Mate-Terminal 1.20 does not allow to move the tabs to the bottom - get used to it.

  - Text not as clear as on local install - still is usable.

  - LibreOffice @ version 5.3.6.1 (vs 6.4.4.2 on Ubuntu 20.04)

  - Emacs @ version 25.3.1 (vs 26.3 on Ubuntu 20.04)
