#+title: How to monitor memory leak
#+author: Chen Rotem Levy
#+email: chen@rotemlevy.name
#+options: ^:nil

* About

  This HOWTO logs memory usage to find an offending process that due to memory leak might hang the system.

  We will write a *Bash* script for the actual monitoring.  This script will be executed by systemd which will execute it and capture its' standard output.

* Setup

** Monitor script

   Create file://usr/local/sbin/memory_nonitor.sh

  #+BEGIN_SRC bash
#/bin/bash

THRESHOLD=1000 # MB
TOP_LINES=40
POLL_TIME=10s

function available_memory() {
    free -mw | awk '$1=="Mem:" {print $8}'
}

function memory_usage() {
    top -bd0.5 -o +%MEM | head -n${TOP_LINES}
}

function main() {
    while true ; do
        if [ $(available_memory) -lt ${THRESHOLD} ] ; then
            memory_usage
        fi
        sleep ${POLL_TIME}
    done
}

main
  #+END_SRC

** Service file

   Create file://lib/systemd/system/memory-monitor.service

   #+BEGIN_SRC conf
[Unit]
Description=Monitor memory usage when it is getting low
ConditionPathExists=!/etc/memrory_monitor_not_to_be_run

[Service]
ExecStart=/bin/bash /usr/local/sbin/memory_monitor.sh
ExecReload=/bin/kill -HUP $MAINPID
KillMode=process
Restart=on-failure
RestartPreventExitStatus=255
Type=simple

[Install]
WantedBy=multi-user.target
Alias=memory-montior.service
   #+END_SRC

** Install

   Enable and start the service with

   #+BEGIN_SRC sh
   sudo systemctl daemon-reload
   sudo systemctl enable memory-montior
   sudo systemctl start memory-montior
   #+END_SRC

* Usage

  To see your log use the `journald` command:

  #+BEGIN_SRC sh
  journalctl -u memory-monitor
  #+END_SRC
